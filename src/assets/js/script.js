var main = function () {

    var
        mood = '',
        arr_mood = ['poo', 'hamburger', 'intero'],
        arr_baby = ['cry', 'gameover', 'pikavnr'],
        div_mood = document.getElementById('div-mood'),
        div_baby = document.getElementById('div-baby'),
        div_audio = document.getElementById('div-audio'),
        btn_feed = document.getElementById('btn-feed'),
        btn_play = document.getElementById('btn-play'),
        btn_game = document.getElementById('btn-game'),
        btn_change = document.getElementById('btn-change'),
        div_audio = document.getElementById('div-audio'),
        div_audio2 = document.getElementById('div-audio2'),
        sec = document.getElementById('sec'),
        min = document.getElementById('min'),
        update_score = document.getElementById('score'),
        second = 0,
        minute = 0,
        progress_bar = document.getElementById('progressbar'),
        btn_order = ['0', '1', '2'];
        audio = $(".soundclip")[0];
        audio1 = $(".soundclip")[1];
        audio2 = $(".soundclip")[2];
        audio3 = $(".soundclip")[3];
        audio4 = $(".soundclip")[4];   

    $("button").mouseenter(function clickevent() {
        audio.play();
    });

    newgame = function () {

        btn_game.classList.add("animated", "flash");
        setTimeout(() => {
            btn_game.classList.remove("animated", "flash");
        }, 700);  
        tempsprog = 10;
        timeg_over = 12000;    
        progress_bar.classList.remove("progress-bar-danger");
        progress_bar.classList.remove("progress-bar-danger2");
        newScore = 0;
        update_score.innerHTML = "0";
        easteregg = 0;
        div_audio2.src = 'assets/sounds/battle.mp3';
        audio1.play();

        if (typeof time != 'undefined') {
            stopChrono();
            resetChrono();
        }

        game();

        time = setInterval(function () {
            second++;
            if (second >= 60) {
                minute++;
                second = 0;
            }
            sec.innerHTML = second + ' sec';
            min.innerHTML = minute + ' min';
        }, 1000);
    },

    eclair = function () {
        easteregg++;
        if (easteregg >= 5) {
            div_baby.innerHTML = '<img class="child" src="assets/images/faces/pikavnr.png">';
            div_audio.src = 'assets/sounds/pikachu.mp3';
        } else {
        }
    },

    stopChrono = function () {
        clearTimeout(time);
    },
    
    resetChrono = function () {    
        second = 0;
        minute = 0;
        sec.innerHTML = "0 sec";
        min.innerHTML = "0 min";
    },

    score = function () {
        newScore++;
        update_score.innerHTML = + newScore;
    },

    missclick = function () {
        swal({
            title: "Ce n'est pas très efficace..",
            button: "Ok"
        }); 
        newScore--;
        update_score.innerHTML = + newScore;
        audio2.play();
    },

    game = function () {

        if (typeof gameover != 'undefined') {
            clearTimeout(gameover);
        }
        
        if (typeof start != 'undefined') {
            clearTimeout(start);
        }

        mood = '';
        div_mood.innerHTML = '';
        div_baby.innerHTML = '<img class="fl-fade-in child" src="assets/images/faces/smile.png">';
        div_audio.src = 'assets/sounds/happy.mp3';

        start = setTimeout(() => {
            mood = random_images(arr_mood);
            div_mood.innerHTML = '<img class="fl-fade-in child animated bounce infinite" src="assets/images/thoughts/' + mood + '.png">';
            div_baby.innerHTML = '<img class="fl-fade-in child animated shake infinite" src="assets/images/faces/' + arr_baby[0] + '.png">';
            div_audio.src = 'assets/sounds/cry.mp3';
            btn_play.style.order = random_images(btn_order);
            btn_feed.style.order = random_images(btn_order);
            btn_change.style.order = random_images(btn_order);
            progress_bar.classList.add("progress-bar-danger");
        }, 2000);

        gameover = setTimeout(() => {
            stopChrono();
            mood = '';
            div_mood.innerHTML = '';
            div_baby.innerHTML = '<img class="animated bounceInDown" src="assets/images/faces/' + arr_baby[1] + '.png">';
            div_audio.src = 'assets/sounds/pikagameover.mp3';
            div_audio2.src = '';
            swal({
                text: "Pikachu a survécu \n " + minute +"min et " + second + "sec, pas mal! \n\nScore : "+ newScore,
                button: "Retente ta chance!"
            }); 
            audio4.play();
        }, timeg_over);
    },

    feed = function () {
        if (mood === 'hamburger') {
            clearTimeout(gameover);
            game();
            score();
            progress_bar.classList.remove("progress-bar-danger");
            easteregg = 0;
            btn_feed.classList.add("animated", "flash");
            setTimeout(() => {
                btn_feed.classList.remove("animated", "flash");
            }, 700);  
        } else {
            missclick();
        }
    },

    play = function () {
        if (mood === 'intero') {
            clearTimeout(gameover);
            game();
            score();
            progress_bar.classList.remove("progress-bar-danger");
            easteregg = 0;
            btn_play.classList.add("animated", "flash");
            setTimeout(() => {
                btn_play.classList.remove("animated", "flash");
            }, 700);  
        } else {
            missclick();
        }
    },

    change = function () {
        if (mood === 'poo') {
            clearTimeout(gameover);
            game();
            score();
            progress_bar.classList.remove("progress-bar-danger");
            easteregg = 0;
            btn_change.classList.add("animated", "flash");
            setTimeout(() => {
                btn_change.classList.remove("animated", "flash");
            }, 700);  
        } else {
            missclick();
        }
    },

    random_images = function (images) {
        return images[Math.floor(Math.random() * images.length)];
    };
    
    div_baby.addEventListener('click', eclair);
    btn_feed.addEventListener('click', feed);
    btn_play.addEventListener('click', play);
    btn_change.addEventListener('click', change);
    btn_game.addEventListener('click', newgame);
}
document.addEventListener('DOMContentLoaded', main);